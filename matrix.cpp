#include "matrix.h"

Matrix::Matrix(){
    this->rows = 0;
    this->cols = 0;    
}

Matrix::Matrix(int rows, int cols, int** array)
{
    this->rows = rows;
    this->cols = cols;
    this->arr = array;
}

Matrix Matrix::Multiply(Matrix a, Matrix b)
{
    Matrix result;
    if(a.cols != b.rows)
        throw "Columns of first matrix must be equal to rows of the second one.";       

    result.rows = a.rows;
    result.cols = b.cols;

    for (int i = 0; i < a.rows; i++)
    {
        for (int j = 0; j < b.cols; j++)
        {
            result.arr[i][j] = 0;
            for (int k = 0; k < b.rows; k++)
            {
                result.arr[i][j] += a.arr[i][k] * b.arr[k][j];
            }
        }
    }

    return result;
}

Matrix Matrix::Transpose()
{
    Matrix result;
    result.rows = this->cols;
    result.cols = this->rows;
    
    for (int i=0; i<this->rows; i++)
        for(int j=0; j<this->cols; j++)
            result.arr[i][j] = this->arr[j][i];

    return result;
}

Matrix Matrix::operator*(Matrix b)
{
    return Multiply(*this, b);
}

