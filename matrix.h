#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <ostream>

class Matrix
{    
    int rows, cols;
    int** arr = nullptr;
    
    Matrix();

    public:          
        Matrix(int, int, int**);
        Matrix operator*(Matrix);
        Matrix Transpose();
        
        static Matrix Multiply(Matrix, Matrix);       

        friend std::ostream& operator<<(std::ostream& out, const Matrix& a);
};


#endif /* _MATRIX_H_ */