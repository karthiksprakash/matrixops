#include <iostream>
#include <stdlib.h>     
#include <time.h>       
#include "matrix.h"

using namespace std;

void run_examples();
void transpose_example(Matrix a);
void multiply_example(Matrix a, Matrix b);
Matrix generate_matrix(int rows, int cols);

int main()
{
    run_examples();   
    return 0;
}

void run_examples()
{
    cout<<"Generating matrices...\n";
    Matrix a = generate_matrix(3, 3);
    Matrix b = generate_matrix(3, 3);
    cout<<"Matrix A:\n"<<a<<"\n";
    cout<<"Matrix B:\n"<<b<<"\n";
    cout <<"\n-------------------------------------------\n";
    
    multiply_example(a, b);   
    transpose_example(a);
        
    cout<<"All done!";
}

void multiply_example(Matrix a, Matrix b)
{    
    cout<<"Multiplication of AxB :\n";
    try{
        cout<<a*b;
    }
    catch(const char* msg){
     cout << msg << endl;
   }
   cout <<"\n-------------------------------------------\n";
}

void transpose_example(Matrix a)
{    
    cout<<"Transpose of A :\n";
    try{
        cout<<a.Transpose();
    }
    catch(const char* msg){
     cout << msg << endl;
   }
   cout <<"\n-------------------------------------------\n";
}

Matrix generate_matrix(int rows, int cols)
{
    /* initialize random seed: */
    srand (time(NULL));

    //Allocate the matrix
    int** matrix = new int*[rows];
    for (int i = 0; i < rows; ++i)
        matrix[i] = new int[cols];
    
    //Initialize matrix
    for (int i=0; i<rows; i++)
        for(int j=0; j<cols; j++) 
            matrix[i][j] = rand() % 10;

    Matrix m(rows, cols, matrix);
    return m;
}

//void print_matrix(Matrix a)
std::ostream& operator<<(std::ostream& out, const Matrix& a)
{
    for (int i=0; i<a.rows; i++)
    {
        out<<"\n\t";
        for(int j=0; j<a.cols; j++)
            out<<a.arr[i][j]<<" ";
    }
    cout<<"Size : "<<a.rows<<" x "<<a.cols<<"\n";
}